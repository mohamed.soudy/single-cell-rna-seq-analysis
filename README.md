# single-cell-RNA-Seq-analysis


## Implementation

The upstream script is inspired by the IKAP paper implementation as in the following figure  

![](https://raw.githubusercontent.com/MohmedSoudy/datasharing/master/giz121fig1.jpeg)

## Upstream analysis
 - Quality control using user-defined thresholds 
 - Normalization using SCT transform
 - Identifying the best number of cell clusters 

## Downstream analysis 
- Cell type assignment using [scType approach](https://www.nature.com/articles/s41467-022-28803-w)
- Cell-Cell communication using [NicheNet approach](https://www.nature.com/articles/s41592-019-0667-5) 
